from fastapi import FastAPI, HTTPException
import pika
import json
from db import SessionLocal, Property
app = FastAPI()

queue_name = 'orchestration_queue'
credentials = pika.PlainCredentials('guest', 'guest')
parameters = pika.ConnectionParameters('localhost',
                                       5672,
                                       '/',
                                       credentials)
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue=queue_name, durable=True)

@app.post("/create-entity/")
async def create_entity(entity: dict):
    source = entity.pop("source")
    print(entity)
    with SessionLocal() as session:
        property_ = Property(**entity)
        session.add(property_)
        session.commit()
    try:
        entity["source"] = source
        message = json.dumps(entity)
        channel.basic_publish(
            exchange='',
            routing_key=queue_name,
            body=message,
            properties=pika.BasicProperties(
                delivery_mode=2,
            )
        )
        return {"status": "success", "message": "Entity creation request enqueued."}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

@app.post("/update-entity/")
async def update_entity(entity_id: str, entity: dict):
    try:
        entity["id"] = entity_id
        message = json.dumps(entity)
        channel.basic_publish(
            exchange='',
            routing_key=queue_name,
            body=message,
            properties=pika.BasicProperties(
                delivery_mode=2,
            )
        )
        return {"status": "success", "message": "Entity update request enqueued."}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)