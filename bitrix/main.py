import requests
import pika
import json
from threading import Thread
from db import SessionLocal, Property

orchestrator_url = 'http://localhost:8000/create-entity/'

queue_name = 'orchestration_queue'

def send_to_orchestrator(data):
    response = requests.post(orchestrator_url, json=data)
    if response.status_code == 200:
        print("Data sent to orchestrator successfully.")
    else:
        print(f"Failed to send data. Status code: {response.status_code}")

def callback(ch, method, properties, body):
    result = json.loads(body)
    print(f"Received result: {result}")
    if result.pop('source') == 'website':
        with SessionLocal() as session:
            property_ = Property(**result)
            session.add(property_)
            session.commit()
    ch.basic_ack(delivery_tag=method.delivery_tag)

def listen_for_results():
    credentials = pika.PlainCredentials('guest', 'guest')
    parameters = pika.ConnectionParameters('localhost',
                                           5672,
                                           '/',
                                           credentials)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()

    channel.queue_declare(queue=queue_name, durable=True)

    channel.basic_consume(queue=queue_name, on_message_callback=callback)

    print('Starting to consume from result queue.')
    channel.start_consuming()

thread = Thread(target=listen_for_results)
thread.start()
